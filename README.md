[Tutorial](https://concoursetutorial.com/)

## Flags

| short | full |
| - | - |
| -t | --target |
| -c | epTODO |

## Immutable Concourse master

What's the best practice for automatically pushing pipeline configurations to the master from source control?

https://github.com/concourse/concourse/issues/1200

## Getting Started

Start Concourse master
```
docker-compose up -d
```

Create target and credentials for the target at ~/.flyrc
```
fly -t <target> login --concourse-url http://127.0.0.1:8080
```

Execute a task
```
fly -t <target> execute -c <path-to-task.yml>
```

List targets
```
fly targets
```

List workers
```
fly -t <target> workers
```

List containers
```
fly -t <target> containers
```

Display authentication status against target
```
fly -t <target> status
```

Logout for all targets
```
fly logout -a
```

Logout for single target
```
fly -t <target> logout
```

Compare Fly & Concourse versions
```
fly -t <target> sync
```
